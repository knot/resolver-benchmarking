====================================
Tools for DNS resolvers benchmarking
====================================

.. contents:: :local:

Shotgun Usage
=============

- Edit `hosts_shotgun`
- Use `scripts/shotgun.sh` to execute the tests, modify variables as needed

Dnsperf Usage
=============

Make sure to initialize the repo with submodules:

.. code-block:: console

  $ git submodule update --init

Specifying the benchmark servers
--------------------------------
In file :code:`hosts` add/remove server or servers into :code:`iterator` and :code:`dnsperf` section.
All ansible playbooks works with groups :code:`iterator` and :code:`dnsperf`.

Remove servers from the cluster
-------------------------------
.. code-block:: console

  $ ansible-playbook remove-from-cluster.yaml

Run benchmark
-------------
:code:`unbound` with default version:

.. code-block:: console

  $ ansible-playbook dnsperf.yaml --extra-vars resolver=unbound --extra-vars "dnsperf_query_file=$QUERY_FILE"

:code:`kresd` with specific version:

.. code-block:: console

  $ ansible-playbook dnsperf.yaml --extra-vars resolver=kresd --extra-vars version=v4.2.2

Return servers to the cluster
-----------------------------
.. code-block:: console

  $ ansible-playbook add-to-cluster.yaml


Configuration of the test
-------------------------

Edit playbook
~~~~~~~~~~~~~
You can edit all varibales in playbook *dnsperf.yaml*.
Focus on this variables:

* :code:`args` in section :code:`kresd`
* :code:`args`, :code:`loop_times` and :code:`dnsperf_query_file` in secrion :code:`dnsperf`

or

Change playbook variables throught playbook arguments
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Change :code:`dnsperf_query_file` for example.

.. code-block:: console

  $ ansible-playbook dnsperf.yaml --extra-vars "dnsperf_query_file=qf/my_dnsperf_query_file"

Example
-------
You can use example :code:`scripts/example.sh`.
But create the test files :code:`qf_1_2M` and :code:`qf_100_2M` before running this example.

Create the test files
~~~~~~~~~~~~~~~~~~~~~
Script :code:`scripts/gen-name-wildcard.py` generate simply query file.

1) Query file with 1 unique domain name (*random_hash.spackovi.net*), type record *A* and 2M of this record in file.
   Saved as query file *qf_1_2M*.

.. code-block:: console

  $ ./scripts/gen-name-wildcard.py -d spackovi.net. -r A -n 2000000 -u 1 && mv qf_1_2M ./files/qf/


2) Query file with 100 unique domain names (100x *random_hash.spackovi.net*), type record *A* and 2M of this record in file.
   Saved as query file *qf_100_2M*.

.. code-block:: console

  $ ./scripts/gen-name-wildcard.py -d spackovi.net. -r A -n 2000000 -u 100 && mv qf_100_2M ./files/qf/

Run the example
~~~~~~~~~~~~~~~
Run the example by command :code:`scripts/example.sh`.

This script run :code:`dnsperf.yaml` playbook with query files :code:`qf_1_2M`, :code:`qf_100_2M` and default resolver and version.
Default resolver defined variable `resolver` in file :code:`dnsperf.yaml`.
Version is defined in role of resolver (*roles/kresd/defaults/main.yaml*).
Graphs are drawn by the script :code:`./scripts/plot.py`.
Note that input of :code:`./scripts/plot.py` script is directory-depend.
For more information explore all scripts mentioned above and :code:`dnsperf.yaml` playbook.

Profiler Usage
==============
Now you can profile just :code:`kresd` with `gperftools <https://gperftools.github.io/gperftools/cpuprofile.html>`_.
Graphs plots `FlameGraph <https://github.com/brendangregg/FlameGraph>`_.

Edit iterator hostname in *hosts* file and run profiler using:

.. code-block:: console

  $ ansible-playbook profiler.yaml

You can change number of samples per second in playbook via :code:`prof_frequency` variable.

Results are stored in *result/profiler* directory.
