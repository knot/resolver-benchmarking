#!/bin/bash

set -o errexit -o nounset -o xtrace

#
# enable/disable benchmark of ...
# type 1 or 0
#
fl_DO=1 # DO bit
fl_DNSSEC=0 # DSSSEC
fl_QM=1 # Query name minimization

#
# Other variables
#
qfdir="files/qf" # must be in 'files' directory because ansible
outdir="results"
playbook="dnsperf.yaml"

qf="qf_10000_2M"
loop=3
resolv=(
	kresd
	unbound
	bind
	powerdns
)
declare -A versions=( [kresd]="v4.2.2" )


[ -f "$qfdir/$qf" ] || ./scripts/gen-name-wildcard.py -d spackovi.net. -r A -n 2000000 -u 10000

#
# DO bit
#
if [ $fl_DO -eq 1 ]; then
	for res in ${resolv[@]}; do
		_ver=""
		[ ${versions[$res]+_} ] && _ver=${versions[$res]}

		if [ ! -z ${_ver} ]; then
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${res}${_ver}/DO_bit/Off \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e perf_args="-c 100" \
				-e resolver=$res -e version=${_ver}
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${res}${_ver}/DO_bit/On \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e perf_args="-c 100 -D" \
				-e resolver=$res -e version=${_ver}
		else
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${res}${_ver}/DO_bit/Off \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e perf_args="-c 100" \
				-e resolver=$res
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${res}${_ver}/DO_bit/On \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e perf_args="-c 100 -D" \
				-e resolver=$res
		fi

		# Plot results
		./scripts/plot.py -i ${outdir}/${res}${_ver}/DO_bit -o ${outdir}/${res}${_ver}/DO_bit/graphs \
			-t "${res}${_ver} - DO bit" -l "DO bit On/Off"
	done
fi

#
# DNSSEC
#
if [ $fl_DNSSEC -eq 1 ]; then
	for res in ${resolv[@]}; do
		_ver=""
		[ ${versions[$res]+_} ] && _ver=${versions[$res]}

		if [ ! -z ${_ver} ]; then
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${res}${_ver}/DNSSEC/Off \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e dnssec=false \
				-e resolver=$res -e version=${_ver}
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${res}${_ver}/DNSSEC/On \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e dnssec=true \
				-e resolver=$res -e version=${_ver}
		else
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${res}${_ver}/DNSSEC/Off \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e dnssec=false \
				-e resolver=$res
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${res}${_ver}/DNSSEC/On \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e dnssec=true \
				-e resolver=$res
		fi

		# Plot results
		./scripts/plot.py -i ${outdir}/${res}${_ver}/DNSSEC -o ${outdir}/${res}${_ver}/DNSSEC/graphs \
			-t "${res}${_ver} - DNSSEC" -l "DNSSEC On/Off"
	done
fi

#
# Query name minimization
#
if [ $fl_QM -eq 1 ]; then
	for (( i=0; i<${#resolv[@]}; i++ )); do
		_ver=""
		[ ${versions[$resolv[i]]+_} ] && _ver=${versions[$resolv[i]]}

		if [ ! -z ${_ver} ]; then
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${resolv[i]}${_ver}/qname_min/Disable \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e qname_min=false \
				-e resolver=${resolv[i]} -e version=${_ver}
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${resolv[i]}${_ver}/qname_min/Enable \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e qname_min=true \
				-e resolver=${resolv[i]} -e version=${_ver}
		else
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${resolv[i]}${_ver}/qname_min/Disable \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e qname_min=false \
				-e resolver=${resolv[i]}
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/${resolv[i]}${_ver}/qname_min/Enable \
				-f qf/$qf  -e dnsperf_loop_times_perf=$loop -e qname_min=true \
				-e resolver=${resolv[i]}
		fi

		# Plot results
		./scripts/plot.py -i ${outdir}/${resolv[i]}${_ver}/qname_min -o ${outdir}/${resolv[i]}/qname_min/graphs \
			-t "${resolv[i]}${_ver} - Query name minimization" \
			-l "Query name minimization On/Off"
	done
fi
