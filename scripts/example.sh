#!/bin/bash
#
# Example how to use ansible playbook dnsperf.yaml and some auxiliary 
# scripts.
#
# NOTE: Please create qf_* file. You can edit and use script 
#       script/gen-name-wildcard.py
#
#
# Uses these files:
#	dnsperf.yaml
#	scripts/run-dnsperf.sh
#	scripts/plot.py
#
set -o errexit -o nounset -o xtrace

OUTDIR="results/example"

[ -d ${OUTDIR} ] || mkdir -p ${OUTDIR}

# Run benchmark for some query files and prepare result for ploting
for QFILE in qf_1_2M qf_100_2M; do
	# extract number of unique names from name's query name files
	UNIQ_NAMES=$(echo $QFILE | cut -d'_' -f2)
	# Run benchmark
	./scripts/run-dnsperf.sh -p dnsperf.yaml -o ${OUTDIR}/${UNIQ_NAMES} \
		-f qf/$QFILE -e dnsperf_loop_times_perf=2 -e perf_args="-c 100"
done

# Plot results
./scripts/plot.py -i ${OUTDIR} -o ${OUTDIR}/graphs -t "Example of benchmarking" -l "Number of unique domains"

