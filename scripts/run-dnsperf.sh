#!/bin/bash

set -o errexit -o nounset -o xtrace

outdir=""
playbook=""
qfile=""
qfdir="files" # because ansible
playbook_args=()
cache_heat=0

function usage {
	echo "Usage:"
	echo -e "\t-h\tDisplay this help message."
	echo -e "\t-p <playbook>\tPlaybook to run."
	echo -e "\t-f <file>\tQuery file (from 'files' directory)."
	echo -e "\t-o <dir>\tOutput directory (save in results/<playbook> or results/<file> directories)."
	echo -e "\t-e <args>\tPlaybook extra arguments."
	echo -e "\t-c yes/no\tPreheat cache (def. \"yes\")."
}

while getopts "hp:f:t:o:e:c:" opt; do
	case ${opt} in
		h)
			usage
			;;
		p)
			playbook="$OPTARG"
			;;
		f)
			qfile="$OPTARG"
			;;

		t)
			tag="$OPTARG"
			;;
		o)
			outdir="$OPTARG"
			;;
		e)
			playbook_args+=("$OPTARG")
			;;
		c)
			[ "$OPTARG" -eq "yes" ] && cache_heat=1
			[ "$OPTARG" -eq "YES" ] && cache_heat=1
			[ "$OPTARG" -eq "no" ] && cache_heat=0
			[ "$OPTARG" -eq "NO" ] && cache_heat=0
			;;

		*)
			echo "Invalid Option: -$OPTARG" 1>&2
			exit 1
			;;
	esac
done


[ -d "${outdir}" ] || mkdir -p "${outdir}"
if [ -z "${outdir}" ]; then
	echo "No output directory"
	exit 1
fi

if [ ! -f $playbook ]; then
	echo "Playbook file \"$playbook\" not found"
	exit 1
fi

if [ ! -f "$qfdir/$qfile" ]; then
	echo "File $qfdir/$qfile not found"
	exit 1
fi


#
# Get dnsperf hostname
#
if [ ! -f hosts ]; then
        echo "host file not found"
        exit 1
fi
HOSTNAME=$(grep -ve '#' hosts | grep -A2 "dnsperf:" | tail -1 | sed -e "s/\s*\([^:]*\):/\1/g")

echo "Run '$playbook' with file '$qfdir/$qfile'"

#
# Aplly new modification
#
playbook_args+=("dnsperf_query_file: $qfile")

# create temporaly yaml file with new configuration
echo "---" >/tmp/$$.yaml
for ((i = 0; i < ${#playbook_args[@]}; i++)); do
	echo "${playbook_args[$i]}" | sed "s/=/: /" >>/tmp/$$.yaml
done

echo "Config| ${playbook_args[@]}"

#
# Run benchmark and create statistics
#
ansible-playbook "${playbook}" --extra-vars @/tmp/$$.yaml
rm /tmp/$$.yaml
# Backup dnsperf.log
mv results/${HOSTNAME}/logs/dnsperf.log "${outdir}/dnsperf.log"
# Read statistics from dnsperf.log - directories tree is important
# Values for y-axis are taken from results.csv
# Values for x-axis are defined by name of direcotry with results.csv ($qfile in this example)
# Exclude first measurements (burn the cache)
./scripts/read-stats-from-log.py -i "${outdir}/dnsperf.log" -e ${cache_heat}\
	>"${outdir}/results.csv"


