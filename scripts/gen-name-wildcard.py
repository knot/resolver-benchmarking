#!/usr/bin/env python3
#
# Generate query file for test.
#
#
import random
import math
import sys
import argparse


def sizeof_fmt(num, suffix=''):
    magnitude = int(math.floor(math.log(num, 1000)))
    val = num / math.pow(1000, magnitude)
    if magnitude > 7:
        return '{:.0f}{}{}'.format(val, 'Y', suffix)
    return '{:.0f}{}{}'.format(val, ['', 'k', 'M', 'G', 'T', 'P', 'E', 'Z'][magnitude], suffix)


def gen_random_hash():
    return ''.join('%032x' % random.getrandbits(128))


def gen_hash_table():
    table = []

    for _ in range(args.num_unames):
        table.append(gen_random_hash())

    return table


if __name__ == '__main__':
    print('Show dot after each 200000 domain names')

    # Parse arguments
    parser = argparse.ArgumentParser(description="Generate query file with random subdomains")
    parser.add_argument('-d', '--dname', required=True,
                        help='Domain name (with root \'.\')')
    parser.add_argument('-r', '--record-type', required=True,
                        help='Graph title')
    parser.add_argument('-n', '--num-names', type=int, required=False,
                        help='Number of names (lines) in query file')
    parser.add_argument('-u', '--num-unames', type=int, required=True,
                        help='Number of unique names (lines) in query file')
    args = parser.parse_args()

    # create output file name
    # join 'qf', number of unique domains and number of domains in human radable format
    if not args.num_names:
        args.num_names = args.num_unames
        output_file = 'qf_' + str(args.num_unames)
    else:
        output_file = 'qf_' + str(args.num_unames) + '_' + sizeof_fmt(args.num_names)

    with open(output_file, 'w') as outf:
        # pregenarate hash table
        hash_table = gen_hash_table()

        for line_num in range(args.num_names):
            if line_num % 200000 == 0:
                sys.stdout.write('.')
                sys.stdout.flush()

            outf.write(hash_table[line_num % args.num_unames] + '.' + args.dname
                       + ' ' + args.record_type + '\n')

    print('\nWrite to file ' + output_file)
