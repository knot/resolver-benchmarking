#!/bin/bash
set -o errexit -o nounset -o xtrace

### START OF CONFIG SECTION ###

# SET number of threads and results dir
srv_threads=8
results_dir=~/playground/shotgun

# SET scenario (see roles/shotgun/files/)
#scenario=udp
#scenario=tcp
scenario=dot
#scenario=doh

# SET whether kresd should use use XDP
kresd_xdp=false

# SET certificate algorithm
tls_cert_type="ed25519"  # options: ed25519, p256, rsa1k, rsa2k, rsa3k, rsa4k, ed448

# SET input data
dataset_name=cc
dataset_length=60r0

# SET client test sets (multiple options possible, separated by space)
clients_testcases="20k"

# SET path to pcap
pcap_dir="/home/respdiff/pcaps/shotgun"

# SET label for easier identification of output dir
label=""  # e.g. oarc33

# SET number of re-runs
runs=1

### RESOLVER SELECTION ###

# SET resolver and version
resolver=kresd
versions=(v5.2.1)

#resolver=unbound
#versions=(1.12.0)

#resolver=bind
#versions=(v9_16_12 v9_11_28 v9_17_10 v9_11_28_S1 v9_16_12_S1)

#resolver=powerdns
#versions=(rec-4.3.4)

#resolver=echo
#versions=(latest)

#resolver=nginx
#versions=(latest)

### END OF CONFIG SECTION ###


for run in $(seq 1 ${runs}); do
    for version in ${versions[@]}; do
        nametag=${resolver}_${version}_${srv_threads}T
        if [ -n "${label}" ]; then
            nametag="${nametag}_${label}"
        fi
        nametag="${nametag}_${scenario}"

        for clients in ${clients_testcases}; do
            dirname="${nametag}_${dataset_name}_${dataset_length}"
            pcap="${pcap_dir}/${dataset_name}_${dataset_length}_${clients}.pcap"
            outdir="${results_dir}/${dirname}/${nametag}-${run}-${clients}C"

            [ -d "${outdir}" ] && (echo "directory ${outdir} already exists!"; exit 2)
            mkdir -p ${outdir}

            rm -f results/shotgun-results/*
            ansible-playbook -i hosts_shotgun shotgun.yaml \
                -e shotgun_pcap="${pcap}" \
                -e shotgun_scenario="${scenario}" \
                -e srv_threads="${srv_threads}" \
                -e resolver="${resolver}" \
                -e version="${version}" \
                -e kresd_xdp="${kresd_xdp}" \
                -e tls_cert_type="${tls_cert_type}" \
                -e results_directory="${outdir}" \
                -e collect_logs=true \
                "$@"

            shopt -s dotglob  # also move hidden .config directory
            mv "${outdir}/results-shotgun"/* "${outdir}/"
            rmdir "${outdir}/results-shotgun"
            shopt -u dotglob
        done
    done
done

echo "---ALL DONE---"
