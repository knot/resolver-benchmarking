#!/usr/bin/env python3

#
# This script takes one directory (input directory) with results and plot it.
# This script depent on directory structure inside this input directory.
# Inside input directory are subdirectories, whitch contains dnsperf.log (stdout
# from multiple runs dnsperf) and results.csv.
# Every subdirectory inside input directory defined one value on x-axis.
# Values for y-axis are taken from results.csv inside this subdirectory.
#
# dnsperf.log - download ansible playbook from benchmark host.
# results.csv - generate script read-stats-from-log.py
#
# For example:
#
#   Directory tree:
#
#     |--- input_directory
#       |--- subdir1
#         |--- dnsperf.log
#         |--- results.csv
#       |--- subdir2
#         |--- dnsperf.log
#         |--- results.csv
#       |--- subdir3
#         |--- dnsperf.log
#         |--- results.csv
#
#   is plotted as:
#
#            y-axis |
#                   |
#      (values      |
#       from        |
#       results.csv |
#                   |
#                   +-----.---------.---------.---- x-axis
#                      subdir1   subdir2   subdir3
#
#
# Read script example.sh for more information how to use this script.
#

import os
import re
import logging
import argparse
import ntpath
import math
import matplotlib
import matplotlib.pyplot as plt


sinames = ['', ' k', ' M', ' G', ' T']


def siname(n):
    try:
        n = float(n)
    except ValueError:
        return n

    siidx = max(0, min(len(sinames)-1,
                       int(math.floor(0 if n == 0 else math.log10(abs(n))/3)))
                )
    return '{:.0f}{}'.format(n / 10**(3 * siidx), sinames[siidx])


def plot(fname, x, y, stack_p, stack_n, ylabel):
    xvalues = x
    fonts = {}  # 'fontname': 'DejaVu Sans'}
    fig = plt.figure(1)
    f = 0.4
    fig.set_size_inches(20*f, 13*f)
    ax = fig.add_subplot(1, 1, 1)
    ax.get_xaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda xidx, p: siname(xvalues[xidx])))
    ax.get_yaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda y, p: siname(y)))
    plt.bar(x, y, width=0.7, color=['#00a2e2'])
    ax.errorbar(x, y, yerr=[stack_n, stack_p], ecolor='black',
                fmt='none', elinewidth=3, capsize=6)
    plt.title(args.title[0], **fonts)
    plt.xlabel(args.label[0], **fonts)
    plt.ylabel(ylabel, **fonts)
    maximum = 0
    for p in ax.patches:
        if p.get_height() > maximum:
            maximum = p.get_height()
    for p in ax.patches:
        height = p.get_height()
        x, y = p.get_xy()
        ax.annotate('{:.0%}'.format(height/maximum), (x, y + height + 0.02*height))
    fig.savefig(args.out_dir[0] + '/' + ntpath.basename(ntpath.dirname(args.out_dir[0])) + fname)
    plt.close()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    # Parse arguments
    parser = argparse.ArgumentParser(description="Plot result from DNS resolver benchmark")
    parser.add_argument('-i', '--in-dir', nargs=1, required=True,
                        help='Input directory with results')
    parser.add_argument('-o', '--out-dir', nargs=1, required=True,
                        help='Output directory with graphs')
    parser.add_argument('-t', '--title', nargs=1, required=True,
                        help='Graph title')
    parser.add_argument('-l', '--label', nargs=1, required=True,
                        help='Graph title')
    args = parser.parse_args()

    x_label = []
    x_val = []
    y_avrg_qps = []
    y_avrg_time = []
    y_med_qps = []
    y_med_time = []
    y_qps_min = 0
    y_qps_max = 0
    y_time_min = 0.0
    y_time_max = 0.0
    y_avrg_qps_stack_p = []
    y_avrg_time_stack_p = []
    y_med_qps_stack_p = []
    y_med_time_stack_p = []
    y_avrg_qps_stack_n = []
    y_avrg_time_stack_n = []
    y_med_qps_stack_n = []
    y_med_time_stack_n = []
    for work_dir in sorted(os.listdir(args.in_dir[0])):
        work_dir = args.in_dir[0] + '/' + work_dir

        if os.path.isdir(work_dir):
            try:
                with open(work_dir + '/results.csv', 'r') as fcsv_stat:
                    x_label.append(work_dir)
                    x_val.append(ntpath.basename(work_dir))
                    for line in fcsv_stat:
                        if re.match('Average', line):
                            y_avrg_qps.append(int(line.split(';')[1]))
                            y_avrg_time.append(float(line.split(';')[2]))

                        if re.match('Median', line):
                            y_med_qps.append(int(line.split(';')[1]))
                            y_med_time.append(float(line.split(';')[2]))

                        if re.match('Min', line):
                            y_qps_min = (int(line.split(';')[1]))
                            y_time_min = (float(line.split(';')[2]))

                        if re.match('Max', line):
                            y_qps_max = (int(line.split(';')[1]))
                            y_time_max = (float(line.split(';')[2]))

                y_avrg_qps_stack_p.append(int(y_qps_max - y_avrg_qps[-1]))
                y_avrg_qps_stack_n.append(int(y_avrg_qps[-1] - y_qps_min))
                y_avrg_time_stack_p.append(float(y_time_max - y_avrg_time[-1]))
                y_avrg_time_stack_n.append(float(y_avrg_time[-1] - y_time_min))
                y_med_qps_stack_p.append(int(y_qps_max - y_med_qps[-1]))
                y_med_qps_stack_n.append(int(y_med_qps[-1] - y_qps_min))
                y_med_time_stack_p.append(float(y_time_max - y_med_time[-1]))
                y_med_time_stack_n.append(float(y_med_time[-1] - y_time_min))
            except FileNotFoundError:
                pass

    if not os.path.exists(args.out_dir[0]):
        os.makedirs(args.out_dir[0])

    if x_val:
        plot(fname='-time-average.svg', x=x_val, y=y_avrg_time, stack_p=y_avrg_time_stack_p,
             stack_n=y_avrg_time_stack_n, ylabel='average time [s]')
        plot(fname='-qps-average.svg', x=x_val, y=y_avrg_qps, stack_p=y_avrg_qps_stack_p,
             stack_n=y_avrg_qps_stack_n, ylabel='average QPS')
        plot(fname='-time-median.svg', x=x_val, y=y_med_time, stack_p=y_med_time_stack_p,
             stack_n=y_med_time_stack_n, ylabel='median time [s]')
        plot(fname='-qps-median.svg', x=x_val, y=y_med_qps, stack_p=y_med_qps_stack_p,
             stack_n=y_med_qps_stack_n, ylabel='median QPS')
