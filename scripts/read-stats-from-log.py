#!/usr/bin/env python3
#
# Read results from input file (dnsperf stdout from multiple run)
# and print statistics to stdout
#
import os
import sys
import logging
import argparse
import re
import statistics


class Stat():
    def __init__(self):
        self.qps = 0
        self.rtime = 0.0
        self.qpass = 0
        self.qlost = 0


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    # Parse arguments
    parser = argparse.ArgumentParser(description="Parser of dnsperf stdout")
    parser.add_argument('-i', '--input-file', nargs=1, required=True,
                        help='Input file (stdout from dnsperf command)')
    parser.add_argument('-e', '--exclude', nargs=1, required=False,
                        help='Exclude this number of run (e.g. 1,2 = exclude first and second run \
                        from input file)')
    args = parser.parse_args()

    if not os.path.isfile(args.input_file[0]):
        logging.error('File %s not found', args.input_file[0])
        sys.exit(1)

    avrg = Stat()
    median = Stat()
    stats_l = Stat()

    with open(args.input_file[0], 'r') as logfile:
        matchs = re.findall(r'\s*Queries per second:\s*([0-9]+).[0-9]+\s*', logfile.read())
        # convert matchs to int and save them into list
        stats_l.qps = list(map(int, matchs))
        # exclude some measurements
        if args.exclude:
            for order in args.exclude[0].split(','):
                if int(order) > 0:
                    logging.debug('Exclude QPS #%s (%s)', order, str(stats_l.qps[int(order)-1]))
                    del stats_l.qps[int(order)-1]
        # caclulate stats
        avrg.qps = statistics.mean(stats_l.qps)
        median.qps = statistics.median(stats_l.qps)
        logging.debug('QPS=%s', str(stats_l.qps))

        logfile.seek(0)
        matchs = re.findall(r'\s*Run time \(s\):\s*([0-9]+.[0-9]+)\s*', logfile.read())
        stats_l.rtime = list(map(float, matchs))
        # exclude some measurements
        if args.exclude:
            for order in args.exclude[0].split(','):
                if int(order) > 0:
                    logging.debug('Exclude Run_time #%d (%s)', order,
                                  str(stats_l.rtime[int(order)-1]))
                    del stats_l.rtime[int(order)-1]
        avrg.rtime = statistics.mean(stats_l.rtime)
        median.rtime = statistics.median(stats_l.rtime)
        logging.debug('Run_time=%s', str(stats_l.rtime))

        logfile.seek(0)
        matchs = re.findall(r'\s*Queries completed:\s*[0-9]+[u]* \(([0-9]+).[0-9]+%\)\s*',
                            logfile.read())
        stats_l.qpass = list(map(int, matchs))
        # exclude some measurements
        if args.exclude:
            for order in args.exclude[0].split(','):
                if int(order) > 0:
                    logging.debug('Exclude Query_pass #%d (%s)', order,
                                  str(stats_l.qpass[int(order)-1]))
                    del stats_l.qpass[int(order)-1]
        # caclulate stats
        avrg.qpass = statistics.mean(stats_l.qpass)
        median.qpass = statistics.median(stats_l.qpass)
        logging.debug('Query_pass=%s', str(stats_l.qpass))

        logfile.seek(0)
        matchs = re.findall(r'\s*Queries lost:\s*[0-9]+[u]* \(([0-9]+).[0-9]+%\)\s*',
                            logfile.read())
        stats_l.qlost = list(map(int, matchs))
        # exclude some measurements
        if args.exclude:
            for order in args.exclude[0].split(','):
                if int(order) > 0:
                    logging.debug('Exclude Query_lost #%d (%s)', order,
                                  str(stats_l.qlost[int(order)-1]))
                    del stats_l.qlost[int(order)-1]
        # caclulate stats
        avrg.qlost = statistics.mean(stats_l.qlost)
        median.qlost = statistics.median(stats_l.qlost)
        logging.debug('Query_lost=%s', str(stats_l.qlost))

    print('Type;Queries [qps];Time [ms]')
    print('Average;%d;%f;%d;%d' % (avrg.qps, avrg.rtime, avrg.qpass, avrg.qlost))
    print('Median;%d;%f;%d;%d' % (median.qps, median.rtime, median.qpass, median.qlost))
    print('Min;%d;%f;%d;%d' % (min(stats_l.qps), min(stats_l.rtime), min(stats_l.qpass),
          min(stats_l.qlost)))
    print('Max;%d;%f;%d;%d' % (max(stats_l.qps), max(stats_l.rtime), max(stats_l.qpass),
          max(stats_l.qlost)))
