#!/bin/bash

set -o errexit -o nounset -o xtrace

outdir="results/example"
qfdir="files/qf"
qfiles=() # are created later
playbook="dnsperf.yaml"

loop=5
resolvers=(
	kresd
	unbound
	bind
	powerdns
)
declare -A versions=( [kresd]="v4.2.2" )


[ -d ${qfdir} ] || mkdir -p ${qfdir}


#
# Generate query files (0-6)
#
echo "Prepare query files ..."
for power in $(seq 0 6); do
	uname=$((10**$power))
	qfname="qf_${uname}"
	echo "Generate file ${qfname}"
	if [ ! -f "${qfdir}/${qfname}" ]; then
		./scripts/gen-name-wildcard.py -d spackovi.net. -r A -u ${uname}
		mv -v ${qfname} ${qfdir} || rm ${qfname}
	fi
	qfiles+=("${qfname}")
done

#
# Run benchmark and plot results for this resolvers
#
for res in ${resolvers[@]}; do
	_ver=""
	[ ${versions[$res]+_} ] && _ver=${versions[$res]}

	# Run benchmark for some query files and prepare result for ploting
	for qfile in "${qfiles[@]}"; do
		echo "Run '$playbook' (resolv: '$res', ver: '${_ver}', file: '$qfile')"
		# extract number of unique names from name's query name files
		unames=$(echo $qfile | cut -d'_' -f2)
		# Run benchmark
		if [ ! -z ${_ver} ]; then
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/$playbook/$res${_ver}/$unames \
				-f qf/$qfile -e dnsperf_loop_times_perf=$loop -e perf_args="-c 100" \
				-e resolver=$res -e version=${_ver}
		else
			./scripts/run-dnsperf.sh -p $playbook -o ${outdir}/$playbook/$res${_ver}/$unames \
				-f qf/$qfile -e dnsperf_loop_times_perf=$loop -e perf_args="-c 100" \
				-e resolver=$res
		fi
	done

	# Plot results
	./scripts/plot.py -i ${outdir}/${playbook}/$res${_ver} -o ${outdir}/${playbook}/$res${_ver}/graphs\
	       -t "${res}:${_ver}" -l "Number of unique queries"
done

