# SPDX-FileCopyrightText: Internet Systems Consortium, Inc. ("ISC")
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import time
import uuid

# pylint: disable=import-error
import ansible_collections.community.general.plugins.callback.unixy


DOCUMENTATION = '''
    author: Petr Špaček (@pspacek)
    name: gitlab_ci_unixy
    type: stdout
    short_description: extend "unixy" output callback with Gitlab CI collapsible headers
    description:
      - This callback extends text output from "unixy" callback with collapsible headers
        specified https://docs.gitlab.com/ee/ci/jobs/#custom-collapsible-sections in a way
        which allows to collapse output from individual tasks.
    extends_documentation_fragment:
      - default_callback
'''


class CallbackModule(ansible_collections.community.general.plugins.callback.unixy.CallbackModule):
    """
    add extra headers to make task logs collapsible in Gitlab CI
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'isc.gitlab_ci_unixy'
    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self):
        self.in_section = None
        super().__init__()

    def _collapsible_header(self, task=None, text=None, collapsed=True):
        self._collapsible_end()

        collapsed_txt = {
            True: '[collapsed=true]',
            False: ''
        }
        if not text and task:
            self._get_task_display_name(task)
            text = self.task_display_name or ' '
        if text:
            self.in_section = str(uuid.uuid4())
            print('\x1b[0Ksection_start:'
                  '{ts:.0f}:'
                  'ansible_section-{name}{collapsed}'
                  '\r\x1b[0K'
                  '{desc}'.format(ts=time.time(), name=self.in_section,
                                  collapsed=collapsed_txt[collapsed],
                                  desc=text))

    def _collapsible_end(self):
        if not self.in_section:
            return

        print('\x1b[0Ksection_end'
              ':{ts:.0f}:'
              'ansible_section-{name}\r\x1b[0K'.format(ts=time.time(), name=self.in_section))
        self.in_section = None

    def v2_playbook_on_task_start(self, task, is_conditional):
        self._collapsible_header(task)
        super().v2_playbook_on_task_start(task, is_conditional)

    def v2_playbook_on_handler_task_start(self, task):
        self._collapsible_header(task)
        super().v2_playbook_on_handler_task_start(task)

    def v2_playbook_on_stats(self, stats):
        self._collapsible_header(text='Play recap', collapsed=False)
        super().v2_playbook_on_stats(stats)
        self._collapsible_end()
