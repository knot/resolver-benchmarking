def sys_cpu_online_path(cpu_id):
    """
    This filter is used along with map() to get a list of paths to write to
    when turning CPUs on/off. It's *much faster* to write the values at once using tee
    rather than looping over a single ansible task.
    """
    return '/sys/devices/system/cpu/cpu{}/online'.format(cpu_id)


class FilterModule:
    def filters(self):  # pylint: disable=no-self-use  # you know nothing, pylint
        return {
            'sys_cpu_online_path': sys_cpu_online_path,
        }
