#!/bin/bash
set -e errexit

if [ -d knot-resolver-ansible ]; then
	cd knot-resolver-ansible 
	git pull
else
	git clone https://gitlab.nic.cz/knot/knot-resolver-ansible
	cd knot-resolver-ansible 
	git submodule update --init --recursive
fi

cd ..

# make sure we start with clear environment
vagrant destroy || :

# start up machines, but don't provision until all are active
vagrant up --parallel --no-provision

# auto-provision with ansible once all machines are up
vagrant provision

